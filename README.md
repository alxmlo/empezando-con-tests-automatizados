# Empezando con tests automatizados

Basado en el articulo de Friends of GO.

Fuente: https://blog.friendsofgo.tech/posts/empezando-con-los-tests-automatizados-en-go/

## Uso

Ejecutar **tool** para tests:

```
go test -v
```

* `-v` modo **verbose**


```
go test -cover ./...
```

* `-cover` genera un reporte de **coverage** (cobertura)
* `./...` ejecución recursiva en directorios

Ejecutar parcialmente un test mediante **subsets**:

```
go test -run TestIsSuperAnimal/gopher ./...
```