package main

import (
	"strings"
)

func IsSuperAnimal(animal string) bool {
	return strings.ToLower(animal) == "gopher"
}

