package main

import "testing"

var isSuperAnimalTests = []struct{
	animal string
	expected bool
}{
	{"elephant", false},
	{"python", false},
	{"gopher", true},
	{"whale", false},
	{"gem", false},
}

func TestIsSuperAnimal(t *testing.T) {
	for _, tt := range isSuperAnimalTests {
		t.Run(tt.animal, func(t *testing.T) {
			// t.Parallel()
			got := IsSuperAnimal("gopher")
			if got != tt.expected {
				t.Errorf("expected: %v; got: %v", tt.expected, got)
			}
		})
	}
}